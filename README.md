# nightwatch-bones
Nightwatch test framework with my favorite pre-loaded features.

## Dive in (Start project and test it)

- `npm install`

- `npm install -g nightwatch`
    
### Native Web (Chrome)
- Start Google sample test
    - `nightwatch --env chrome --group googleExampleTest`
    


https://selenium-release.storage.googleapis.com/3.4/selenium-server-standalone-3.4.0.jar

https://dl.google.com/linux/direct/google-chrome-stable_curr‌​ent_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get -f install
google-chrome --version

sudo apt-get install chromium-browser
/usr/bin/chromium-browser

sudo apt-get install nodejs-legacy
node -v

sudo apt-get install git
git --version

sudo apt-get install npm
npm -v

sudo apt-get install default-jdk
java -version

sudo npm install -g nightwatch
nightwatch -v

git clone https://github.com/shane-reaume/nightwatch-bones
npm install chromedriver --save
npm install nightwatch --save
npm install nightwatch-html-reporter --save
npm install platform --save
npm install

nightwatch --env chrome --group googleExampleTest

sudo apt-get install google-chrome-stable
sudo apt-get install google-chrome-stable
sudo sh -c "echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google.list"
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

sudo npm install chromedriver -g

https://gist.github.com/addyosmani/5336747
http://nightwatchjs.org/gettingstarted/#chromedriver
https://github.com/nightwatchjs/nightwatch/issues/1439

https://stackoverflow.com/questions/30401135/nightwatch-use-chromedriver
https://github.com/nightwatchjs/nightwatch/issues/797
https://stackoverflow.com/questions/39630559/jenkins-nightwatch-email-report
https://github.com/mycargus/docker-grid-nightwatch
